// code from https://github.com/svyatik/GoodParallax/
export default {
  init(param) {
    if (typeof param === 'undefined') {
      param = {}
    }

    param = {
      speed: typeof param.speed !== 'undefined' ? param.speed : 5,
      className: typeof param.className !== 'undefined' ? param.className : 'parallax',
    }

    var parallax = document.getElementsByClassName(param.className);

    for (var i = 0; parallax.length > i; i++) {
      var img_data = parallax[i].dataset.img;
      if (typeof img_data !== 'undefined') {
        parallax[i].style.backgroundImage = 'url(' + img_data + ')';
        parallax[i].style.backgroundSize = 'cover';
      }

      // Add other styles for background
      parallax[i].style.backgroundPosition = 'center 0';
      parallax[i].style.backgroundAttachment = 'fixed';
    }

    // Add scroll event listener
    var rafTimer;
    var self = this;
    window.onscroll = function () {
      cancelAnimationFrame(rafTimer);
      rafTimer = requestAnimationFrame(function(){
        self.changeParallaxPosition(parallax, param.speed);
      });
    };
    // Run function
    this.changeParallaxPosition(parallax, param.speed);
  },
  changeParallaxPosition(parallax, speed) {
    //In the loop count and apply new position to everyone element
    for (var i = 0; parallax.length > i; i++) {
      // Count the new background position
      var bgScroll = -((window.scrollY - parallax[i].offsetTop) / speed);
      // Save new background position to variable
      var bgPosition = 'center ' + bgScroll + 'px';
      // Set new background position to element
      parallax[i].style.backgroundPosition = bgPosition;
    }
  },
}
