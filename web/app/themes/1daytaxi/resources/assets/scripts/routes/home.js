export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS

    // Mobile color switcher
		function checkCarSelector(){
			var mq = window.matchMedia( "(min-width: 768px)" );
			if (mq.matches) {
				$('.selector-car').show();
			} else {
				$('.selector-yellow-car, .selector-toyellow-car').hide();
				$('.color-selector a').removeClass('active');
				$('.color-selector a.selector-white').addClass('active');
			}
		}
		checkCarSelector();
		//window.addEventListener('resize', function(){
		//	checkCarSelector();
		//}, true);
		$('.color-selector a').click(function(e){
			e.preventDefault();
			$(this).parents('.color-selector').find('a').removeClass('active');
			$(this).parents('.container').find('.selector-car').hide();
			$('.'+$(this).data('selector')).show();
			$(this).addClass('active');
		});
  },
};
