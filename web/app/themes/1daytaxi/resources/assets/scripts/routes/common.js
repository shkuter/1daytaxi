import parallax from "../parallax";
export default {
  init() {
    // JavaScript to be fired on all pages

    /* Phone switcher */
    var day = $('.day-phone');
    var night = $('.night-phone');
    var hour = new Date().getUTCHours() + 3;
    if ((hour >= 6) && (hour < 17)) {
      day.show();
      night.hide();
    } else {
      day.hide();
      night.show();
    }

    /* Pseudo-links to toggle content */
    $('a[data-slidetoggle]').click(function (e) {
      e.preventDefault();
      $($(this).data('slidetoggle')).slideToggle();
      $(this).toggleClass('opened');
    });

    /* Top link */
    $(window).scroll(function() {
      var scrollonscreen = $(window).scrollTop() + $(window).height();
      if (scrollonscreen > $(window).height() + 350) {
          $('#top-link').show();
      } else {
          $('#top-link').hide();
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    var mq = window.matchMedia( "(min-width: 550px)" );
    if (mq.matches) {
      parallax.init();
    }
  },
};
