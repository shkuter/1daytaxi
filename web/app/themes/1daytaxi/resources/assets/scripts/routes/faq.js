export default {
  init() {
    // JavaScript to be fired on the FAQ page
    /* Accordion */
    $('.accordion-header').click(function () {
      //e.preventDefault();
      var $this = $(this);
      $('.accordion-header').removeClass('open');
      if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp();
        $this.removeClass('open');
      } else {
        $this.parent().parent().find('li .accordion-inner').removeClass('show');
        $this.parent().parent().find('li .accordion-inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle();
        $this.addClass('open');
      }
    });
  },
  finalize() {
    // JavaScript to be fired on the FAQ page, after the init JS
    var $selectedEl;
    if (window.location.hash) {
      var hash = window.location.hash;
      $selectedEl = $('a[href="' + hash + '"]');
    } else {
      $selectedEl = $('.accordion-header:eq(0)');
    }
    $selectedEl.trigger('click');
  },
};
