@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.content-page')
        <div class="hero parallax" data-img="@asset('images/hero-background.jpg')" id="hero">
            <div class="bg-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h1>
                                <span>Лицензия на такси</span>.<br>
                                Официально<span>.</span><br>
                                <a href="#terms"><span>Быстрее всех</span></a>.
                            </h1>
                        </div>
                        <div class="col-lg-4">
                            <div>
                                <p class="action">Оставьте заявку и&nbsp;получите лицензию на&nbsp;такси <span>в</span>&nbsp;<a href="#terms">рекордно быстрые сроки</a><span>!</span></p>
                                @php
                                    echo do_shortcode( '[contact-form-7 id="19" title="Завяка"]' );
                                @endphp
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="terms parallax" data-img="@asset('images/hand-background.jpg')" id="terms">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Какого цвета ваш автомобиль?</h2>
                        <ul class="color-selector">
                            <li>
                                <a href="#white-car" class="selector-white" data-selector="selector-white-car">Белый</a>
                            </li>
                            <li>
                                <a href="#yellow-car" class="selector-yellow active" data-selector="selector-yellow-car">Желтый</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col selector-car selector-white-car" id="white-car">
                        <img class="img-fluid" src="@asset('images/white-8.png')" alt="Белый">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Обычный&nbsp;срок</td>
                                    <td><strong>8-10</strong> раб. дн.</td>
                                    <td><strong>3500</strong> руб</td>
                                </tr>
                                <tr class="special">
                                    <td>Быстрый срок</td>
                                    <td><strong>3-5</strong> раб. дней</td>
                                    <td><strong>10000</strong> руб</td>
                                </tr>
                            </tbody>
                        </table>
                        <a class="btn btn-primary" href="#popup-form" data-toggle="modal" data-target="#popup-form">Получить лицензию</a>
                    </div>
                    <div class="col selector-car selector-yellow-car" id="yellow-car">
                        <img class="img-fluid" src="@asset('images/yellow-8.png')" alt="Желтый">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Обычный срок</td>
                                    <td><strong>10</strong> раб. дней</td>
                                    <td><strong>3000</strong> руб</td>
                                </tr>
                                <tr class="special">
                                    <td>Быстрый срок</td>
                                    <td>от <strong>1</strong> раб. дня</td>
                                    <td><strong>8000</strong> руб</td>
                                </tr>
                            </tbody>
                        </table>
                        <a class="btn btn-primary" href="#popup-form" data-toggle="modal" data-target="#popup-form">Получить лицензию</a>
                    </div>
                    <div class="col-12">
                        <p class="link-to-skin"><a href="#skin">Ваша машина другого цвета?</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="advantages" id="advantages">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-md-4">
                        <span class="oi oi-clock"></span>
                        <h2>Сверхбыстро</h2>
                        <p>Мы делаем лицензию на такси <a href="#terms">от 1 рабочего дня</a> по Москве и <a href="#terms">от 3 рабочих дней</a>&nbsp;лицензия по Московской области!</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-badge"></span>
                        <h2>Легально</h2>
                        <p>Выданная лицензия на такси полностью соответствует букве закона. Вы сможете убедиться в легитимности нашей лицензии на такси на сайте Департамента Транспорта (официальный ресурс).</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-lock-locked"></span>
                        <h2>Без предоплаты</h2>
                        <p>Прежде чем купить лицензию, мы не просим клиентов вносить аванс. Вы платите только за готовую лицензию такси, по факту наличия записи о ней в реестре, а оригинала – в ваших руках.</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-calendar"></span>
                        <h2>Надолго</h2>
                        <p>Выданная лицензия такси действительна в течение 5 лет, после чего ее можно переоформить на другой автомобиль или продлить за 1 рабочий день в нашей компании.</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-circle-check"></span>
                        <h2>Удобно</h2>
                        <p>Вы получаете все преимущества официальной деятельности такси — проезд по выделенным полосам, въезд к вокзалам и метро Москвы и Московской области.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="skin parallax" data-img="@asset('images/hand-background.jpg')" id="skin">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Оклейка + Лицензия на такси</h2>
                        <p>Ваша машина не белая и не желтая? Воспользуйтесь нашим комплексным предложением при получении лицензии такси!</p>

                        <p>Выберите цвет оклейки:</p>
                        <ul class="color-selector">
                            <li>
                                <a class="selector-white active" data-selector="selector-towhite-car" href="#white-skin">Белый</a>
                            </li>
                            <li>
                                <a class="selector-yellow" data-selector="selector-toyellow-car" href="#yellow-skin">Желтый</a>
                            </li>
                        </ul>		
                    </div>
                    <div class="col selector-car selector-towhite-car" id="white-skin">
                        <img class="img-fluid" src="@asset('images/blackwhite-8.png')" alt="В белый">
                        <p>Оклейка в <strong>белый + лицензия Московской области</strong><br>
                            Пленка Oracal (Германия), гарантия 1 год<br>
                            цена <strong>от 14000 руб</strong></p>
                        <a class="btn btn-primary" href="#popup-request" data-toggle="modal" data-target="#popup-request">Узнать подробности</a>
                    </div>
                    <div class="col selector-car selector-toyellow-car" id="yellow-skin">
                        <img class="img-fluid" src="@asset('images/blackyellow-8.png')" alt="В желтый">
                        <p>Оклейка в <strong>желтый + лицензия для Москвы</strong><br>
                            Пленка Oracal (Германия), гарантия 1 год<br>
                            цена <strong>от 15000 руб</strong></p>
                        <a class="btn btn-primary" href="#popup-request" data-toggle="modal" data-target="#popup-request">Узнать подробности</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="documents" id="documents">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Необходимые документы для получения лицензии на такси</h2>
                        <p><strong>Минимум документов и удобство отправки!</strong> Сканы / фотографии документов можно отправлять на Email / Viber / WhatsApp.</p>
                    </div>
                    <div class="col-md-6">
                        <img class="img-fluid" src="@asset('images/passport-8.png')">
                        <h3>Копия паспорта</h3>
                        <p>(первый разворот с фото)</p>
                    </div>
                    <div class="col-md-6">
                        <img class="img-fluid" src="@asset('images/sts-8.png')">
                        <h3>Копия СТС</h3>
                        <p>(обе стороны)</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4><a href="#ip-ooo" data-slidetoggle="#ip-ooo">Получаете лицензию<br> на ИП или ООО?</a></h4>
                        <div class="js-slidetoggle" id="ip-ooo">
                            <p>Получаете лицензию на ИП или ООО?</p>
                            <p>Дополнительно попросим вас следующие документы:</p>
                            <p>— <strong>копия свидетельства о государственной регистрации</strong> юридического лица или физического лица в качестве ИП;</p>
                            <p>— <strong>копия свидетельства о постановке на учет в налоговом органе</strong> физического / юридического лица по месту его жительства / нахождения.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4><a href="#not-owner" data-slidetoggle="#not-owner">Автомобиль не в вашей собственности?</a></h4>
                        <div class="js-slidetoggle" id="not-owner">
                            <p>Дополнительно попросим вас следующие документы:   </p>                     
                            <p>— <strong>копия договора лизинга</strong> или <stront>копия договора аренды</stront> транспортного средства, либо <strong>копия нотариально заверенной доверенности</strong> на право распоряжения автомобилем.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="proc" id="proc">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Лицензия на такси от 1DayTaxi: как мы работаем</h2>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-camera-slr"></span>
                        <p>Вы качественно фотографируете или сканируете все необходимые документы, перечисленные выше на сайте.</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-envelope-closed"></span>
                        <p>Отправляете сканы/фото нам удобным для вас способом:
                            
                            на email: <a href="mailto:info@1daytaxi.ru">info@1daytaxi.ru</a>
                            в Viber/WhatsApp: @php echo do_shortcode( '[ps_phones]' ); @endphp</p>
                    </div>
                    <div class="col-md-4">
                        <span class="oi oi-document"></span>
                        <p>Мы проверяем полученные документы и начинаем работу в день их получения! Через 1 день сообщаем вам о готовности лицензии такси (подлинность можно проверить на государственном сайте) и согласуем время и место получения ее оригинала.</p>
                    </div>
                    <div class="col-12">
                        <a href="#popup-request" class="btn btn-primary" data-toggle="modal" data-target="#popup-request">Оставить заявку</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts vcard" id="contacts">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Наши контакты</h2>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span class="oi oi-location"></span>
                        <p><strong>Адрес</strong></p>
                        <p class="adr">
                            <a href="https://yandex.ru/maps/-/CBq2IJvH-B" target="_blank">
                                <span class="locality">г. Москва</span>,<br>
                                <span class="street-address">Воробьевское шоссе 6</span>
                            </a>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span class="oi oi-phone"></span>
                        <p><strong>Телефон</strong></p>
                        @php echo do_shortcode( '[ps_phones]' ); @endphp
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span class="oi oi-envelope-closed"></span>
                        <p><strong>Email</strong></p>
                        <p>
                            <a class="email" href="mailto:info@1daytaxi.ru">info@1daytaxi.ru</a>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <span class="oi oi-clock"></span>
                        <p><strong>Время работы</strong></p>
                        <p>
                            <span class="workhours">Ежедневно&nbsp;9:00 —&nbsp;00:00</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <noindex>
                        <h2>Лицензия на такси в Москве: быстро, надежно, легально</h2>
                        <p>Работа в системе пассажироперевозок &ndash; довольно прибыльный бизнес. Однако возможность работать в такси должна быть подтверждена документально: помимо права управлять автомобилем такси, необходимо иметь соответствующее разрешение. Получить лицензию на такси в Москве можно несколькими способами, но далеко не все из них доступны и приемлемы по срокам и стоимости.</p>
                        <p>Если оформить документ на такси нужно быстро, без проволочек и доплат, советуем обращаться к профессионалам. Мы поможем получить лицензию на такси в Москве на абсолютно законных основаниях.</p>
                        <h2>Закажите лицензию на такси &ndash; сэкономьте время</h2>
                        <p>В глобальной сети довольно много рекомендаций по поводу того, как оформить лицензию на такси самостоятельно, не имея свидетельства индивидуального предпринимателя и не обращаясь к специалистам. На первый взгляд здесь нет ничего особенного. Помимо соответствующего заявления в рекомендованную администрацией организацию подаются дубликаты:</p>
                        <ul>
                            <li>документов на машину, в которых указано ФИО владельца транспортного средства;</li>
                            <li>паспорта;</li>
                            <li>в отдельных случаях &ndash; копия доверенности.</li>
                        </ul>
                        <p>Срок рассмотрения заявления в таком случае составляет не менее месяца. Не хотите ждать столько времени? Конечно, можно купить лицензию на такси, но если вы всерьез намерены работать, покупка лицензии на такси &ndash; не самый удачный вариант. К тому же подлинность документов может оказаться под сомнением.</p>
                        <p>Обратившись в нашу компанию через сайт, вы значительно сэкономите время. Наши специалисты помогут оперативно и совершенно легально сделать лицензию на такси в Москве в рекордно короткие сроки: от одного до десяти дней!</p>
                        <h2>Получаем лицензию на такси оперативно</h2>
                        <p>Оформление любых официальных документов, особенно таких, как лицензия на такси (Москва или регионы &ndash; не имеет значения), может обернуться немалыми расходами. Как минимум, придется потратиться на:</p>
                        <ul>
                            <li>дорогу;</li>
                            <li>оформление копий и дубликатов;</li>
                            <li>оплату дополнительных услуг вне очереди;</li>
                            <li>помощь юристов, нотариусов и так далее.</li>
                        </ul>
                        <p>Чтобы сберечь собственные деньги и нервы, гораздо быстрее, разумнее и проще заказать услугу у профессионалов. В некоторых случаях такой вариант обойдется даже дешевле. Такая лицензия на такси, цена которой от трех тысяч рублей, полностью соответствует всем нормам и стандартам разрешительных документов для работы в такси.</p>
                        <h2>Лицензия на такси с ИП и без</h2>
                        <p>Многие автовладельцы, которые хотят заработать, используют собственный автомобиль, отдают предпочтение получению лицензии на такси без юридического лица.</p>
                        <p>Правда, оформить разрешение для такси без трудоустройства сложно и дорого. Упростить задачу и получить лицензию на такси поможет профессиональная помощь.</p>
                        <h2>Получаем лицензию на такси</h2>
                        <p>Возможность хоть немного сэкономить на уплате весьма обременительных налогов, организации документооборота и обязательных пенсионных отчислениях &ndash; весьма заманчивый вариант. Но необходимо знать, что в перечне обязательных бумаг для получения разрешения на участие в системе пассажироперевозок, в любом случае фигурирует лицензия на такси. Она нужна, даже если вы не индивидуальный предприниматель.</p>
                        <p>Предлагаем заказать лицензию на такси, и уже через 1-3 дня все необходимое для работы будет у вас на руках.</p>
                        </noindex>
                </div>
            </div>
        </div>
  @endwhile
@endsection
