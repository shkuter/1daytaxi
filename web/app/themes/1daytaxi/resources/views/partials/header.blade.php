<div class="container header flex-wrap">
  <a class="logo" href="{{ home_url('/') }}"><img src="@asset('images/logo.png')" alt="{{ get_bloginfo('name', 'display') }}"></a>
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-menu', 'container' => false]) !!}
  @endif
  <div class="callback">
    <div class="ps_phone">
      @php
        echo do_shortcode( '[ps_phones]' );
      @endphp
    </div>
    <div class="callback_btn">
      <a href="#popup-callback" class="btn" data-toggle="modal" data-target="#popup-callback">Заказать звонок</a>
    </div>
  </div>
</div>
<!-- Modals -->
<div class="modal fade" id="popup-callback" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3>Заказать звонок</h3>
          @php
              echo do_shortcode('[contact-form-7 id="17" title="Обратный звонок"]');
          @endphp
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="popup-form" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3>Получение лицензии</h3>
          @php
              echo do_shortcode('[contact-form-7 id="20" title="Получение лицензии"]');
          @endphp
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="popup-request" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3>Завяка</h3>
          @php
              echo do_shortcode('[contact-form-7 id="19" title="Завяка"]');
          @endphp
      </div>
    </div>
  </div>
</div>
