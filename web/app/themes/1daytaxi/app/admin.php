<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * Admin menu page
 */
add_action( 'admin_menu',  __NAMESPACE__ . '\\ps_admin_menu' );
function ps_admin_menu() {
    //$role = get_role( 'subscriber' );
    //$role->add_cap('change_schedule');
	add_menu_page( __('Расписание'), __('Расписание'), 'manage_options', 'phone-schedule', __NAMESPACE__ . '\\ps_admin_page', 'dashicons-phone', 1  );
}
function ps_admin_page() {
    $options = get_option( 'ps_settings' );
    $days = array(
        1 => 'Пн',
        2 => 'Вт',
        3 => 'Ср',
        4 => 'Чт',
        5 => 'Пт',
        6 => 'Сб',
        7 => 'Вс'
    );
    ?>
    <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="page_options" value="ps_settings" />
        <div class="wrap">
            <h1>Расписание</h1>
            <table class="form-table" style="width:100px;">
                <tbody>
                    <tr>
                        <th>Менеджер&nbsp;1:</th>
                        <td><input name="ps_settings[name][1][name]" placeholder="Имя" type="text" value="<?= $options['name'][1]['name'] ?>" /></td>
                        <td><input name="ps_settings[name][1][phone]" placeholder="Телефон" type="text" value="<?= $options['name'][1]['phone'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Менеджер&nbsp;2:</th>
                        <td><input name="ps_settings[name][2][name]" placeholder="Имя" type="text" value="<?= $options['name'][2]['name'] ?>" /></td>
                        <td><input name="ps_settings[name][2][phone]" placeholder="Телефон" type="text" value="<?= $options['name'][2]['phone'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Менеджер&nbsp;3:</th>
                        <td><input name="ps_settings[name][3][name]" placeholder="Имя" type="text" value="<?= $options['name'][3]['name'] ?>" /></td>
                        <td><input name="ps_settings[name][3][phone]" placeholder="Телефон" type="text" value="<?= $options['name'][3]['phone'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Менеджер&nbsp;4:</th>
                        <td><input name="ps_settings[name][4][name]" placeholder="Имя" type="text" value="<?= $options['name'][4]['name'] ?>" /></td>
                        <td><input name="ps_settings[name][4][phone]" placeholder="Телефон" type="text" value="<?= $options['name'][4]['phone'] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Менеджер&nbsp;5:</th>
                        <td><input name="ps_settings[name][5][name]" placeholder="Имя" type="text" value="<?= $options['name'][5]['name'] ?>" /></td>
                        <td><input name="ps_settings[name][5][phone]" placeholder="Телефон" type="text" value="<?= $options['name'][5]['phone'] ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>Дневной телефон</b></td>
                        <td><b>Ночной телефон</b></td>
                    </tr>
                    <?php
                    foreach ($days as $key => $day) {
                        $row = '<tr><th>' . $day . ':</th>';
                        $row .= '<td><select name="ps_settings[dayphone][' . $key . ']">';
                        foreach ($options['name'] as $key2 => $option) {
                            if ($option['name'] != '' && $option['phone'] != ''){
                                if ($option['phone'] == $options['dayphone'][$key]) {
                                    $selected = 'selected';
                                } else {
                                    $selected = '';
                                }
                                $row .= ('<option ' . $selected . ' value="' . $option['phone'] . '">' . $option['name'] . '</option>');
                            }
                        }
                        $row .= '</select></td>';
                        $row .= '<td><select name="ps_settings[nightphone][' . $key . ']">';
                        foreach ($options['name'] as $key2 => $option) {
                            if ($option['name'] != '' && $option['phone'] != ''){
                                if ($option['phone'] == $options['nightphone'][$key]) {
                                    $selected = 'selected';
                                } else {
                                    $selected = '';
                                }
                                $row .= ('<option ' . $selected . ' value="' . $option['phone'] . '">' . $option['name'] . '</option>');
                            }
                        }
                        $row .= '</select></td>';
                        $row .= '</tr>';
                        echo $row;
                    }
                    ?>
                    <tr>
                        <th><label for="code">Код для вставки:</label></th>
                        <td><input id="code" type="text" value="[ps_phone]" /></td>
                    </tr>
                </tbody>
            </table>
            <p><input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"></p>
    </form>
    <?php
}
