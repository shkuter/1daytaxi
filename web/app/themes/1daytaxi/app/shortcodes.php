<?php

namespace App;

// Shortcode for phones
add_shortcode( 'ps_phones',  __NAMESPACE__ . '\\phones_func' );
function phones_func( $atts ){
    $options = get_option( 'ps_settings' );
    $day_number = intval(date('N'));
	return '<span class="night-phone phone"><a href="tel:' . $options['nightphone'][$day_number] . '" class="tel">' . $options['nightphone'][$day_number] . '</a></span> <span class="day-phone phone"><a href="tel:' . $options['dayphone'][$day_number] . '" class="tel">' . $options['dayphone'][$day_number] . '</a></span>';
}
